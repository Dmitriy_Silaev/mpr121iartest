/*
* The MIT License (MIT)
*
* Copyright (c) 2015 Marco Russi
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/


/* touch and OOR statuses */
#define TS1		0x00 //00
#define TS2 		0x01 //00
#define OORS1 		0x02 //00
#define OORS2 		0x03 //00

/* filtered data */
#define E0FDL 		0x04 //be
#define E0FDH 		0x05 //02
#define E1FDL 		0x06 //c9
#define E1FDH 		0x07 //02
#define E2FDL 		0x08 //ca
#define E2FDH 		0x09 //00
#define E3FDL 		0x0A //c8
#define E3FDH 		0x0B //02
#define E4FDL 		0x0C //ca
#define E4FDH 		0x0D //02
#define E5FDL 		0x0E //cc
#define E5FDH 		0x0F //02
#define E6FDL 		0x10 //cc
#define E6FDH 		0x11 //02
#define E7FDL 		0x12 //c7
#define E7FDH 		0x13 //02
#define E8FDL 		0x14 //c4
#define E8FDH 		0x15 //02
#define E9FDL 		0x16 //c7
#define E9FDH 		0x17 //02
#define E10FDL 		0x18 //c5
#define E10FDH 		0x19 //02
#define E11FDL 		0x1A //cc
#define E11FDH 		0x1B //02
#define E12FDL 		0x1C //c7
#define E12FDH 		0x1D //02

/* baseline values */
#define E0BV 		0x1E //af
#define E1BV 		0x1F //b2
#define E2BV 		0x20 //b2
#define E3BV 		0x21 //b1
#define E4BV 		0x22 //b2
#define E5BV 		0x23 //b3
#define E6BV 		0x24 //b3
#define E7BV 		0x25 //b1
#define E8BV 		0x26 //b1
#define E9BV 		0x27 //b1
#define E10BV 		0x28 //b1
#define E11BV 		0x29 //b3
#define E12BV 		0x2A //b1

/* general electrode touch sense baseline filters */
/* rising filter */
#define MHDR 		0x2B //04
#define NHDR 		0x2C //01
#define NCLR 		0x2D //00
#define FDLR 		0x2E //00

/* falling filter */
#define MHDF 		0x2F //05
#define NHDF 		0x30 //01
#define NCLF 		0x31 //05
#define FDLF 		0x32 //10

/* touched filter */
#define NHDT 		0x33 //01
#define NCLT 		0x34 //ff
#define FDLT 		0x35 //ff

/* proximity electrode touch sense baseline filters */
/* rising filter */
#define MHDPROXR 	0x36 //01
#define NHDPROXR 	0x37 //01
#define NCLPROXR 	0x38 //00
#define FDLPROXR 	0x39 //00

/* falling filter */
#define MHDPROXF 	0x3A //01
#define NHDPROXF 	0x3B //01
#define NCLPROXF 	0x3C //28
#define FDLPROXF 	0x3D //50

/* touched filter */
#define NHDPROXT 	0x3E //00
#define NCLPROXT 	0x3F //ff
#define FDLPROXT 	0x40 //ff

/* electrode touch and release thresholds */
#define E0TTH 		0x41 //1f
#define E0RTH 		0x42 //05
#define E1TTH 		0x43 //10
#define E1RTH 		0x44 //05
#define E2TTH 		0x45 //11
#define E2RTH 		0x46 //05
#define E3TTH 		0x47 //17
#define E3RTH 		0x48 //05
#define E4TTH 		0x49 //12
#define E4RTH 		0x4A //05
#define E5TTH 		0x4B //17
#define E5RTH 		0x4C //05
#define E6TTH 		0x4D //11
#define E6RTH 		0x4E //05
#define E7TTH 		0x4F //15
#define E7RTH 		0x50 //05
#define E8TTH 		0x51 //15
#define E8RTH 		0x52 //05
#define E9TTH 		0x53 //11
#define E9RTH 		0x54 //05
#define E10TTH 		0x55 //1d
#define E10RTH 		0x56 //05
#define E11TTH 		0x57 //17
#define E11RTH 		0x58 //05
#define E12TTH 		0x59 //01
#define E12RTH 		0x5A //02

/* debounce settings */
#define DTR 		0x5B //21

/* configuration registers */
#define AFE1 		0x5C //bf
#define AFE2 		0x5D //e1
#define ECR 		0x5E //3c //00 - Baseline tracking enabled, initial baseline value is current value in baseline value register
                                  //11 - Run Mode with ELE0~ELE11combined for proximity detection enabled
                                  //11xx - Run Mode with ELE0~ ELE11 for electrode detection enabled
/* electrode currents */
#define CDC0		0x5F //1e
#define CDC1 		0x60 //3a
#define CDC2 		0x61 //36
#define CDC3 		0x62 //32
#define CDC4 		0x63 //2d
#define CDC5 		0x64 //37
#define CDC6 		0x65 //34
#define CDC7 		0x66 //30
#define CDC8 		0x67 //2b
#define CDC9 		0x68 //3c
#define CDC10 		0x69 //3b
#define CDC11 		0x6A //36
#define CDC12 		0x6B //23

/* electrode charge times */
#define CDT01 		0x6C //00
#define CDT23 		0x6D //1f
#define CDT45 		0x6E //1f
#define CDT67 		0x6F //1f
#define CDT89 		0x70 //1f
#define CDT1011 	0x71 //1f
#define CDT11 		0x72 //04

/* GPIO */
#define CTL0		0x73 //00
#define CTL1 		0x74 //00
#define DAT 		0x75 //00
#define DIR 		0x76 //00
#define EN 		0x77 //00
//#define SET 		0x78 //00
//#define CLR 		0x79 //00
#define TOG 		0x7A //00

/* auto-config */
#define ACCR0		0x7B //9b
#define ACCR1		0x7C //00
#define USL		0x7D //c4
#define LSL		0x7E //7f
#define TL		0x7F //b0

/* soft reset */
#define SRST		0x80 //00

/* PWM */
#define PWM0		0x81 //00
#define PWM1		0x82 //00
#define PWM2		0x83 //00
#define PWM3 		0x84 //00


#define I2C2_DEVICE_ADDRESS_1 0x5A
#define I2C2_DEVICE_ADDRESS_2 0x5B

/* End of file */

